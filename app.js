// Setup imports

const express = require("express");
const mongoose = require("mongoose");
const dotenv = require("dotenv");

// This allows us to use all the routes defined in the "taskRoutes.js" file
const taskRoutes = require("./routes/taskRoutes.js");



// express setup
const app = express();
const port = 3001;

// Initialize dotenv
dotenv.config();

// Middlewares
app.use(express.json());
app.use(express.urlencoded({extended: true}));

// mongoose setup
mongoose.connect(`mongodb+srv://lhemarcanete:${process.env.MONGODB_PW}@224canete.vd2heps.mongodb.net/s36?retryWrites=true&w=majority`, 
    {
        useNewUrlParser: true,
        useUnifiedTopology: true
    }
) //para smooth ang connection to mongodb


let db = mongoose.connection;
db.on('error', () => console.error(`Connection Error.`));
db.once('open', () => console.log(`Connected to MongoDB!`));


// Main URI
// http://localhost:3001/tasks/<endpoint>
// NOTE: Make sure to assign a specific endpoint for every database collection. In this case, since we are manipulating the 'tasks' collection in MongoDb, then we are specifying an endpoint with specific routes and controllers for that collection.
app.use("/tasks", taskRoutes);


app.listen(port, () => console.log(`Server is running at port: ${port}`));